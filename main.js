const btnsCode = document.querySelectorAll('.button-code');

document.addEventListener('keypress', function (e) {
    btnsCode.forEach(function (btn) {
        btn.classList.remove('active');
    });

    const currentKeyBlock = document.querySelector(`[data-key-code=${e.code}]`);

    if ( currentKeyBlock ) currentKeyBlock.classList.add('active');
})

